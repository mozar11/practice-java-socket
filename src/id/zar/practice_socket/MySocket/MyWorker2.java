/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.zar.practice_socket.MySocket;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 *
 * @author zar
 */
public class MyWorker2 {

    /**
     * @param args the command line arguments
     */
//    public static void main(String[] args) {
//        MyWorker2 ll = new MyWorker2();
//        ll.connect();
//    }

    public void connect() {
        try {
            String host = "lionair.co.id";
//            String host = "ipinfo.io";
            int timeOut = 1000 * 1;
            //https://secure2.lionair.co.id/lionairibe2/OnlineBooking.aspx?depart=CGK&dest.1=SUB&trip_type=one%20way&date.0=5Oct&date.1=4Oct&persons.0=1&persons.1=0&persons.2=0&date_flexibility=undefined

            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(host, 80), timeOut);

            OutputStream os = socket.getOutputStream();

            String req = "GET / HTTP/1.1\n"
                    + "Host: " + host + "\n"
                    + "Connection: keep-alive\n"
                    + "Upgrade-Insecure-Requests: 1\n"
                    + "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36\n"
                    + "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\n"
                    + "Accept-Encoding: gzip, deflate, sdch\n"
                    + "Accept-Language: en-US,en;q=0.8,ms;q=0.6,id;q=0.4\n\n";

            os.write(req.getBytes());
            System.out.println(req);
            InputStream is = socket.getInputStream();
            byte[] buffer = new byte[8];
            int readStatus = 0;
            do {
                readStatus = is.read(buffer);
                if (readStatus > 0) {
                    String response = new String(buffer, StandardCharsets.UTF_8);
                    System.out.print(response);
                }

//                System.out.println("read status = " + readStatus);
            } while (readStatus >= 8);
            socket.close();
        } catch (Exception x) {
            x.printStackTrace();
        }
    }
}
