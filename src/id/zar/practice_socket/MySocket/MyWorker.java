/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.zar.practice_socket.MySocket;

import id.zar.practice_socket.MyHttp.HttpRequestHeader;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 *
 * @author zar
 */
public class MyWorker implements Runnable {

    private Socket socket;
    private HttpRequestHeader httpRequestHeader;

    public MyWorker(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        System.out.println("worker is starting");
        try {
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            readInput(inputStream);
            httpRequestHeader = new HttpRequestHeader(requestHeader);
            writeOutput(outputStream);
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("worker is done");
    }
    private String requestHeader = "";
    private int bufferSize = 64;
    private boolean endOfHeader = false;

    private void readInput(InputStream inputStream) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        byte[] buffer = new byte[bufferSize];
        int readLength = 0;
        System.out.println("read request:");
        while ((readLength = inputStream.read(buffer)) > 0) {
            String read = byteToString(buffer, readLength);
            stringBuilder.append(read);
            System.out.print(read);
            if (readLength < bufferSize) {
                break;
            }
        }
        requestHeader = stringBuilder.toString();
        System.out.println("read request done");
    }

    private String byteToString(byte[] buffer, int length) {
//        int length = buffer.length;
        char[] last2Req = new char[4];
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char c = (char) buffer[i];
            builder.append(c);
            last2Req[0] = last2Req[1];
            last2Req[1] = last2Req[2];
            last2Req[2] = last2Req[3];
            last2Req[3] = c;
            if (last2Req[2] == '\r' && last2Req[3] == '\n') {
                if (last2Req[0] == last2Req[2] && last2Req[1] == last2Req[3]) {
                    endOfHeader = true;
                }
            }
        }
        return builder.toString();
    }

    private void writeOutput(OutputStream outputStream) throws Exception {
        System.out.println("write response");
        String result = "wkwk";
        outputStream.write(result.getBytes());
        System.out.println("write response done");
    }
}
