/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.zar.practice_socket.MySocket;

import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author zar
 */
public class MyServer implements Runnable{
    public static void main(String... args){
        int argsLength = args.length;
//        if(argsLength<2){
//            System.out.println("arguments are missing.");
//            return;
//        }
        int port = 9876;
        boolean portIsSet = false;
        for(int i=0; i<argsLength; i++){
            if(args[i].equals("-port") && i+1<argsLength){
                port = Integer.parseInt(args[i+1]);
                i++;
                portIsSet = true;
            }
        }
        if(portIsSet == false){
            System.out.println("port is set to default = " + port);
        }
        MyServer server = new MyServer(port);
        server.run();
    }
    
    private int port = 9876;
    private boolean running = true;
    public  MyServer(int port){
        this.port = port;
    }

    @Override
    public void run() {
        try{
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("ready to accept connection at port " + port);
            while(running){
                Socket accept = serverSocket.accept();
                System.out.println("inbound connection");
                new Thread(new MyWorker(accept)).start();
            }
        }catch (Exception x){
            x.printStackTrace();
        }
        System.out.println("Server is shutting down");
    }
}
