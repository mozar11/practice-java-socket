/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.zar.practice_socket.MyHttp;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author zar
 */
public class HttpRequestHeader {

    private String headerString = "",
            requestString = "",
            postData = "",
            method = "",
            document = "/",
            dataBoundaries = "";
    private static final String METHOD_POST = "POST",
            METHOD_GET = "GET";
    private Map<String, String> headerMap = new HashMap<>(),
            dataMap = new HashMap<>();

    public HttpRequestHeader(String request) {
        this.requestString = request;
        tryParse();
    }

    private void tryParse() {
        int headerPos = requestString.indexOf("\r\n\r\n");
        headerString = requestString.substring(0, headerPos);
        postData = requestString.substring(headerPos + 4);
        requestString = "";
        processHeader();
        processData();
    }

    private void processHeader() {
        String[] header1 = headerString.split("\r\n");
        for (int i = 0, i2 = header1.length; i < i2; i++) {
            String header2 = header1[i];
            if (i == 0) {
                String[] header3 = header2.split(" ");
                if (header3[0].equals(METHOD_GET)) {
                    method = METHOD_GET;
                } else if (header3[0].equals(METHOD_POST)) {
                    method = METHOD_POST;
                }
                document = header3[1];
            } else {
                String[] header3 = header2.split(": ");
                if (header3.length > 1) {
                    headerMap.put(header3[0], header3[1]);
                    if (header3[0].equals("Content-Type")) {
                        String[] contentType = header3[1].split("; ");
                        for (String content : contentType) {
                            int pos = content.indexOf("boundary=");
                            if (pos >= 0) {
                                dataBoundaries = content.substring(9);
                            }
                        }
                    }
                }
            }
        }
    }

    private void processData() {
        if (dataBoundaries.equals("")) {
            return;
        }
        if (method.equals(METHOD_POST) == false) {
            return;
        }
        String[] data1 = postData.split("--"+dataBoundaries+"\r\n");
        for(String data2 : data1){
            String[] data3 = data2.split("\r\n");
            if(data3.length>2){
                int pos = data3[0].indexOf("name=");
                String key = "";
                if(pos>=0){
                    key = data3[0].substring(pos+5);
                    if(key.length()>2){
                        key = key.substring(1,key.length()-1);
                    }
                    dataMap.put(key, data3[2]);
                }
            }
        }
    }
}
